ELK role
=========

Installs Elasticsearch Logstash Kibana on Debian/Ubuntu servers.

Dependencies
------------

`tyumentsev4.elastic_repo` role to install elastic repository.

Example Playbook
----------------
```yml
- hosts: all
  become: true
  roles:
    - tyumentsev4.elk
```
